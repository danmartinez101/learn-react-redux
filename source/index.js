import React, { Component } from 'react';
import Root from './app';

React.render(
  <Root/>,
  document.getElementById('root')
);
