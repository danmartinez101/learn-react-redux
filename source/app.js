import React, { Component } from 'react';
import hotify from 'react-hotify';
import { connect, Provider } from 'react-redux';
import { createStore } from 'redux';

import immutable from 'immutable';

const todoDraftUpdated = 'todoDraftUpdated';
const todoDraftCleared = 'todoDraftCleared';
const todoCreated = 'todoCreated';
const todoUpdated = 'todoUpdated';
const todoEditingFinished = 'todoEditingFinished';
const todoEditingStarted = 'todoEditingStarted';
const todoEditingCancelled = 'todoEditingCancelled';

const actions = {
  clearTodoDraft() {
    return {type: todoDraftCleared};
  },
  updateTodoDraft(title) {
    return {type: todoDraftUpdated, payload: {title}};
  },
  createTodo(title) {
    const id = Math.random();
    return {type: todoCreated, payload: {id, title}};
  },
  updateTodo(id, title) {
    return {type: todoUpdated, payload: {id, title}}
  },
  finishEditingTodo(id, title) {
    return {type: todoEditingFinished, payload: {id, title}}
  },
  startEditingTodo(id) {
    return {type: todoEditingStarted, payload: {id}};
  },
  cancelEditingTodo(id) {
    return {type: todoEditingCancelled, payload: {id}};
  }
}

const initialState = immutable.fromJS({draft:{title:''},todos:{}});

function reducer(state = initialState, action) {

  let actionHandlers = {
    [todoDraftCleared]: (payload) => ({draft: {title:''}}),
    [todoDraftUpdated]: (payload) => ({draft: payload}),
    [todoCreated]: (payload) => ({todos:{[payload.id]:payload}}),
    [todoUpdated]: (payload) => ({todos:{[payload.id]: payload}}),
    [todoEditingFinished]:  (payload) => ({todos:{[payload.id]: {...payload, isBeingEdited: false}}}),
    [todoEditingStarted]: (payload) => {
      const current = state.toJS().todos[payload.id];
      const next = {...current, isBeingEdited: true, archives: [...current.archives||[], current]};
      return {
        todos: {
          [payload.id]: next
        }
      };
    },
    [todoEditingCancelled]: (payload) => {
      const current = state.toJS().todos[payload.id];
      const next = current.archives.pop();
      console.log(next);
      return {
        todos: {
          [payload.id]: {...next, isBeingEdited: false}
        }
      };
    }
  };
  let handleAction = actionHandlers[action.type] || ()=>({});
  return state.mergeDeep(handleAction(action.payload));
}

const store = createStore(reducer);

@hotify('TodoDraft')
class TodoDraft {
  render() {
    return (
      <div>
        <input value={this.props.draft.title}
          onChange={::this.onUpdate}
          onBlur={::this.onUpdate}
          onKeyDown={::this.onKeyDown}
          />
      </div>
    )
  }
  onUpdate(e) {
    this.props.dispatch(actions.updateTodoDraft(e.target.value));
  }

  onKeyDown(e) {
    if(e.keyCode === 13) {
      this.props.dispatch(actions.createTodo(e.target.value));
      this.props.dispatch(actions.clearTodoDraft());
    }
    if(e.keyCode === 27) {
      this.props.dispatch(actions.clearTodoDraft());
    }
  }

}

@hotify('TodoEdit')
class TodoEdit {

  componentDidMount() {
    const titleInput = React.findDOMNode(this.refs.title);
    titleInput.setSelectionRange(
      titleInput.value.length,
      titleInput.value.length
    );
  }

  render() {
    return (
      <div>
        <input ref="title"
          value={this.props.todo.title}
          onChange={::this.onUpdate}
          onBlur={::this.onUpdate}
          onKeyDown={::this.onKeyDown}
          autoFocus={true}
          />
      </div>
    )
  }
  onUpdate(e) {
    this.props.dispatch(actions.updateTodo(this.props.todo.id, e.target.value));
  }
  onKeyDown(e) {
    if (e.keyCode === 13) {
      this.props.dispatch(actions.finishEditingTodo(this.props.todo.id, e.target.value));
    }
    if(e.keyCode === 27) {
      this.props.dispatch(actions.cancelEditingTodo(this.props.todo.id));
    }
  }
}

@hotify('TodoView')
class TodoView {
  render() {
    const todo = this.props.todo;
    return todo.isBeingEdited
    ? <TodoEdit todo={todo} dispatch={this.props.dispatch}/>
    : <div>{todo.title}<button onClick={::this.onEdit}>edit</button></div>
  }
  onEdit(e) {
    this.props.dispatch(actions.startEditingTodo(this.props.todo.id))
  }
}

@connect(state => state.toJS())
@hotify('index')
class Index extends Component {
  render() {
    return (
      <div>
        <TodoDraft draft={this.props.draft} dispatch={this.props.dispatch} />
        {Object.keys(this.props.todos).map(key => <TodoView todo={this.props.todos[key]} dispatch={this.props.dispatch}/>)}
      </div>
    )
  }
}

export default
class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        {() => <Index {...this.props}/>}
      </Provider>
    )
  }
}


store.subscribe(() => {
  console.log(store.getState().toJS());
});
