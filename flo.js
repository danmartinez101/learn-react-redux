var flo = require('fb-flo'),
    path = require('path'),
    fs = require('fs'),
    exec = require('child_process').exec;

var server = flo(
  './source',
  {
    port: 8888,
    host: 'localhost',
    verbose: false,
    glob: [
      '**/*.js',
      '**/*.css'
    ]
  },
  function resolver(filepath, callback) {
    callback({
      resourceURL: filepath,
      contents: fs.readFileSync('source/' + (filepath)).toString(),
      reload: false,
      update: function(_window, _resourceURL) {
        System.delete('http://localhost:3000/'+_resourceURL);
        System.import('http://localhost:3000/'+_resourceURL).then(function () {
          console.log("Updated:", _resourceURL);
        });
      }
    });
  }
);
console.log('flo is running on localhost:8888');
